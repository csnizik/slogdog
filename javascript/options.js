// Save options to chrome.storage
function save_options() {
  let optionWatermark = document.getElementById('optionWatermark').checked;
  let optionColorize = document.getElementById('optionColorize').checked;
  chrome.storage.sync.set({
    sd_watermark: optionWatermark,
    sd_colorize: optionColorize
  }, function () {
    let button = document.getElementById('save');
    button.textContent = 'Saved';
    setTimeout(function () {
      status.textContent = '';
      chrome.tabs.getCurrent(function(tab) {
        chrome.tabs.remove(tab.id);
      })
    }, 900);
  });
}

// Retrieve options to set default values of form
function restore_options() {
  chrome.storage.sync.get(null, function(results) {
    let watermark = results.sd_watermark;
    let colorize = results.sd_colorize;
    document.getElementById('optionWatermark').checked = watermark;
    document.getElementById('optionColorize').checked = colorize;
  });
}

document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);
