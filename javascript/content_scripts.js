chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
  const environment = msg.environment;
  const watermark = msg.watermark;
  const colorize = msg.colorize;
  let backgroundColor;
  let textColor;
  let text;
  let width;
  let top;
  switch(environment) {
    case "local":
      text = watermark === true && "DEV" || "";
      backgroundColor = colorize === true && "rgb(97, 0, 0)" || "inherit";
      textColor = colorize === true && "rgb(255, 153, 153, 0.2)" || "rgba(153, 192, 255, 0.2)";
      width = '110px';
      top = '52';
      break;
    case "qa":
      text = watermark === true && "QA" || "";
      backgroundColor = colorize === true && "rgb(0, 73, 97)" || "inherit";
      textColor = colorize === true && "rgb(0, 96, 128, 1)" || "rgba(153, 192, 255, 0.2)";
      width = '100px';
      top = '44';
      break;
    case "stg":
      text = watermark === true && "STG" || "";
      backgroundColor = colorize === true && "rgb(48, 0, 97)" || "inherit";
      textColor = colorize === true && "rgb(179, 255, 153, 0.2)" || "rgba(153, 192, 255, 0.2)";
      width = '115px';
      top = '52';
      break;
    default:
      text = "";
  }
  changeHeader(watermark, colorize, text, backgroundColor, textColor, width, top);
});

function changeHeader(wm, cl, text, backgroundColor, textColor, width, top) {
  let header = document.getElementsByClassName('header_top');
  if (wm === true && header[0] && typeof(header[0]) === 'object' && header[0].style.backgroundImage === '') {
    headerToWatermark = header[0];
    headerToWatermark.style['backgroundImage'] = watermark(text, textColor, width, top);
  }
  if (cl === true && header[0] && typeof(header[0]) === 'object' && header[0].style.backgroundColor !== backgroundColor) {
    headerToColorize = header[0];
    headerToColorize.style['backgroundColor'] = backgroundColor;
  }
}

function watermark(text, textColor, width, top) {
  return "url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' height='50px' width='" + width + "'>" + "<text transform='translate(25, " + top + ") rotate(-22)' fill='" + textColor + "' font-size='48px'>" + text + "</text></svg>\")";
}
