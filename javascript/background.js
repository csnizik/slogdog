const SESSION_COOKIE_NAME = '_amex_session';

chrome.runtime.onInstalled.addListener(function() {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([
            {
                conditions: [
                    new chrome.declarativeContent.PageStateMatcher({
                        pageUrl: { hostContains: 'amex' },
                    })
                ],
                actions: [ new chrome.declarativeContent.ShowPageAction() ]
            }
        ]);
    });
});

chrome.cookies.onChanged.addListener( function(cookies) {
    if (cookies.cookie.name === SESSION_COOKIE_NAME && cookies.cause === 'explicit' && cookies.removed === false){
        chrome.storage.sync.get(null, function(result) {
            let currentEnvName =
              cookies.cookie.domain === 'amex-local-dev.iseatz.com' && 'local' ||
              cookies.cookie.domain === 'amex-qa.iseatz.com' && 'qa' ||
              cookies.cookie.domain === 'amex-stg.iseatz.com' && 'staging' || 'prod';
            let storedSessionName = `current_${currentEnvName}_session`;
            let sessId = cookies.cookie.value;
            if (result.hasOwnProperty([storedSessionName]) && result[storedSessionName].hasOwnProperty("value") && result[storedSessionName]["value"] === sessId) {
                return; // Avoiding storing a session that has already been stored ensures that the original created date for this session will persist
            }
            let session = {
                created: calculateCreated(cookies.cookie.expirationDate),
                domain: cookies.cookie.domain,
                environment: currentEnvName,
                name: cookies.cookie.name,
                path: cookies.cookie.path,
                secure: cookies.cookie.secure,
                slog_url: currentEnvName === 'prod' && `https://slog-view-dark.iseatz.com/list/${sessId}?env=${currentEnvName}` || `https://slog-view-qa.iseatz.com/list/${sessId}?env=${currentEnvName}`,
                value: sessId
            };
            let filteredSessions = [];
            if (result.hasOwnProperty('storedSessions')) {
                let dateNow = new Date().valueOf();
                // TODO: add a max sessions filter to avoid overloading storage
                filteredSessions = result.storedSessions.filter(sess => (sess.value !== session.value) && (dateNow - sess.created.value < (1000*60*60*24*7))); // Filter out stored sessions > 7 days old
            }
            filteredSessions.push(session);
            // filteredSessions = []; // Uncomment this to reset stored sessions.
            chrome.storage.sync.set({
                [storedSessionName]: session,
                storedSessions: filteredSessions
            });
        })
    }
});

chrome.webNavigation.onCompleted.addListener(function() {
    chrome.storage.sync.get(null, function(results) {
        passEnvironmentToContent(results);
    })
});

function calculateCreated(expiryDate){
    let date = new Date(Math.trunc(expiryDate * 1000) - 8.64e+7);
    let createdDate = {
        string: "<strong>" + date.toDateString() + "</strong><br/>" + date.toLocaleTimeString('en-US'),
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate(),
        value: date.valueOf()
    };
    return createdDate;
}

function passEnvironmentToContent(results){
    let watermark = results.sd_watermark;
    let colorize = results.sd_colorize;
    let environment = null;
    if (watermark === true || colorize === true) {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            if (tabs.length === 0) {
                return;
            }
            const tabId = tabs[0].id;
            const url = tabs[0].url;
            environment = (url.indexOf("amex-local-dev") !== -1 && "local") || (url.indexOf("amex-qa") !== -1 && "qa") || (url.indexOf("amex-stg") !== -1 && "stg") || null;
            if (environment !== null) {
                let message = {
                    environment: environment,
                    watermark: watermark,
                    colorize: colorize
                };
                chrome.tabs.sendMessage(tabId, message);
            }
        });
    }
}

let filters = {
    url: [{urlContains: 'amex'}]
};

if (chrome.webNavigation.onDOMContentLoaded) {
    chrome.webNavigation.onDOMContentLoaded.addListener(onNavigate, filters);
    // console.log('abc');
}

function onNavigate(details){
    // let a = 'b';
    // console.log('bcd');
}
