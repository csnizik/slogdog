const SESSION_COOKIE_NAME = '_amex_session';

let viewSlog = select('#viewSlog');
let newSession = select('#newSession');
let showHistory = select('#showHistory');
let showSettings = select('#showSettings');
let deleteHistory = select('#deleteHistory');
let deleteDialog = select('#deleteDialog');

function select(selector) {
    return document.querySelector(selector);
}

function openInNewTab(url) {
    const win = window.open(url, '_blank');
    win.focus();
}

deleteHistory.onclick = () => {
    deleteDialog.style.visibility = "visible";
    let modalContents = "<div class='mdl-button mdl-js-button mdl-button--raised mdl-button--accent' id='confirmDelete'>Confirm Delete</div><div class='mdl-button mdl-button--colored' id='cancelDelete'>Cancel</div>";
    deleteDialog.innerHTML = modalContents;
    let confirmDelete = select('#confirmDelete');
    let cancelDelete = select('#cancelDelete');
    confirmDelete.onclick = () => {
        chrome.storage.sync.clear(function() {
            let modalContents = "<div class='mdl-grid'><div class='mdl-cell mdl-cell--2-col mdl-cell--1-offset'><p>DELETED!</p></div></div>";
            deleteDialog.innerHTML = modalContents;
            setTimeout(function() {
                doClose()
            }, 900);
        })
    };
    cancelDelete.onclick = () => {
        deleteDialog.style.visibility = "hidden";
        doClose();
    };
};


viewSlog.onclick = () => {
    getCurrentTabUrl(function(url) {
        if (url === undefined) {
            alert('Please refresh this tab and try again.');
            return;
        }
        const parsedUrl = parseUrl(url);
        const currentDomain = parsedUrl.host;
        switch(currentDomain) {
            case 'amex-local-dev.iseatz.com':
                chrome.storage.sync.get(['current_local_session'], function (result) {
                    slogUrl = result.current_local_session.slog_url;
                    openInNewTab(slogUrl);
                });
                break;
            case 'amex-stg.iseatz.com':
                chrome.storage.sync.get(['current_staging_session'], function (result) {
                    slogUrl = result.current_staging_session.slog_url;
                    openInNewTab(slogUrl);
                });
                break;
            case 'amex-qa.iseatz.com':
                chrome.storage.sync.get(['current_qa_session'], function (result) {
                    slogUrl = result.current_qa_session.slog_url;
                    openInNewTab(slogUrl);
                });
                break;
            default:
                chrome.storage.sync.get(['current_prod_session'], function (result) {
                    slogUrl = result.current_prod_session.slog_url;
                    openInNewTab(slogUrl);
                });
        }
    })
};

newSession.onclick = () => {
    chrome.tabs.query({"active": true, "currentWindow": true}, (tabs) => {
        chrome.cookies.remove({"url": tabs[0].url, "name": SESSION_COOKIE_NAME}, function() {
            let urlComponents = parseUrl(tabs[0].url);
            let returnUrl = `${urlComponents.protocol}://${urlComponents.host}${urlComponents.port}${urlComponents.path}`;
            chrome.tabs.update({"url": returnUrl}, function() {
                chrome.cookies.set({"url": returnUrl, "name": "slogdog", "value": "refresh"}, function() {
                    doClose();
                })
            })
        });
    });
};

showHistory.onclick = () => {
    openInNewTab('history.html');
};

showSettings.onclick = () => {
    openInNewTab('options.html');
};

function getCurrentTabUrl(callback) {
    let queryInfo = {
        active: true,
        currentWindow: true
    };
    chrome.tabs.query(queryInfo, (tabs) => {
        var tab = tabs[0];
        var url = tab.url;
        callback(url);
    });
};

function parseUrl(url) {
    let parsedUrl = url.split(/^(http|https|ftp)?(?:[\:\/]*)([a-z0-9\.-]*)(?:\:([0-9]+))?(\/[^?#]*)?(?:\?([^#]*))?(?:#(.*))?$/i);
    let path = parsedUrl[4];
    if (path.includes("/", 1)) { // Searching for a second instance of a backslash so we can strip everything after it
        basePath = path.substr(0, path.indexOf("/", 1));
    } else {
        basePath = path;
    }
    if (parsedUrl[3] === undefined) {
        parsedUrl[3] = "";
    }
    let urlComponents = {
        protocol: parsedUrl[1],
        host: parsedUrl[2],
        port: parsedUrl[3].length > 0 && ":" + parsedUrl[3] || "",
        path: basePath
    };
    return urlComponents;
}

function doClose() {
    self.close();
}
