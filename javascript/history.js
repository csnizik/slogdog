let showSettings = select('#showSettings');

function select(selector) {
  return document.querySelector(selector);
}

function openInNewTab(url) {
  const win = window.open(url, '_blank');
  win.focus();
}

function buildTables() {
  chrome.storage.sync.get(null, function(results) {
    let localRows = '';
    let stagingRows = '';
    let qaRows = '';
    let prodRows = '';
    if (results.hasOwnProperty('storedSessions') && typeof(results.storedSessions) === "object" && results.storedSessions.length > 0) {
      const sessionsCount = results.storedSessions.length;
      for (let [key, session] of Object.entries(results.storedSessions)) {
        if (session === null) {
          continue;
        }
        index = sessionsCount - key;
        environment = session.environment;
        created = session.created;
        value = session.value;
        slog_url = session.slog_url;
        switch(environment) {
          case "local":
            localRows = "<tr><td class='mdl-data-table__cell--non-numeric'>" + created.string + "</td><td class='mdl-data-table__cell--non-numeric'><a href='" + slog_url + "' target='_blank'>" + value.substr(0, 8) + "...</a></td></tr>" + localRows;
            break;
          case "qa":
            qaRows = "<tr><td class='mdl-data-table__cell--non-numeric'>" + created.string + "</td><td class='mdl-data-table__cell--non-numeric'><a href='" + slog_url + "' target='_blank'>" + value.substr(0, 8) + "...</a></td></tr>" + qaRows;
            break;
          case "staging":
            stagingRows = "<tr><td class='mdl-data-table__cell--non-numeric'>" + created.string + "</td><td class='mdl-data-table__cell--non-numeric'><a href='" + slog_url + "' target='_blank'>" + value.substr(0, 8) + "...</a></td></tr>" + stagingRows;
            break;
          case "prod":
            prodRows = "<tr><td class='mdl-data-table__cell--non-numeric'>" + created.string + "</td><td class='mdl-data-table__cell--non-numeric'><a href='" + slog_url + "' target='_blank'>" + value.substr(0, 8) + "...</a></td></tr>" + prodRows;
            break;
          default:
            break;
        }
      }
    }
    if (localRows.length > 0) {
      document.getElementById("localRows").innerHTML = localRows;
    }
    if (stagingRows.length > 0) {
      document.getElementById("stagingRows").innerHTML = stagingRows;
    }
    if (qaRows.length > 0) {
      document.getElementById("qaRows").innerHTML = qaRows;
    }
    if (prodRows.length > 0) {
      document.getElementById("prodRows").innerHTML = prodRows;
    }
  });
}

document.addEventListener('DOMContentLoaded', buildTables);

showSettings.onclick = () => {
  openInNewTab('options.html');
};
