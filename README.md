# SlogDog

SlogDog is a Chrome extension that is configured for use on pages within the following domains:

- http://amex-local-dev.iseatz.com/*
- https://amex-qa.iseatz.com/*
- https://amex-stg.iseatz.com/*

Additionally, the unpublished Developer Version works on:

- https://www.amextravel.com/*
- https://travel.americanexpress.com/*

### SlogDog functions

To access the basic functions, click the SlogDog icon on an amex_web page. (The icon will be colorized/active on amex_web pages; on other pages, it will be grey/inactive.)

Clicking the icon will open a popup with these functions:

1. **SlogDog Options**: clicking the gear icon opens the Options page
1. **Fetch Slog**: opens a new tab with the current slog for the session in the active tab
1. **Start New Session**: ends the session for the active tab and begins a new session
1. **View Slog History**: opens the Session History page with lists of current and previous sessions for each environment: Local, Staging, QA and Prod (Developer Version only)

### Other functions

Clicking the gear icon brings you to the Options page, where you can currently turn on/off these optional features:

1. **Colorize Environment**: changes the color of the blue header on Amex pages according to the current environment
1. **Watermark Environment**: adds a watermark to the blue header on Amex pages according to the current environment

### How to install

1. Clone or download the repo to your local machine.
1. In Google Chrome, go to `chrome://extensions`.
1. In the top right corner of the browser, make sure "Developer mode" is turned on.
1. Click "Load unpacked" and select the SlogDog directory.
1. Go to any page on one of the domains listed above. (If you already have one of these domains loaded in a current tab, you may need to refresh the page after installation.)
1. Click the SlogDog icon to the right of Chrome's address bar; a new tab should open with the slog data for the current session.

Questions or feedback are welcome! Use the "Give Feedback" link in bitbucket or Slack me `@Christopher Snizik`...***or make a pull request.***
